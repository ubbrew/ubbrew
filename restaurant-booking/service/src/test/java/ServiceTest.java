import com.model.dao.BookingDAO;
import com.model.dao.RestaurantDAO;
import com.model.dao.UserDAO;
import com.model.entities.Booking;
import com.model.entities.Restaurant;
import com.model.entities.User;
import com.service.exception.ServiceException;
import com.service.services.Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ServiceTest {
    @Mock
    public RestaurantDAO restaurantDAO;
    @Mock
    public UserDAO userDAO;
    @Mock
    public BookingDAO bookingDAO;

    private User user = new User();
    private Restaurant restaurant = new Restaurant();
    private int noOfSeats;

    @InjectMocks
    public Service service;

    @Test
    public void testSaveRestaurant() throws ServiceException {
        doAnswer((Answer<Restaurant>) invocationOnMock -> null).when(restaurantDAO).save(any(Restaurant.class));
        service.saveRestaurant(new Restaurant());
    }

    @Test
    public void testUpdateRestaurant() {
        doAnswer((Answer<Restaurant>) invocationOnMock -> null).when(restaurantDAO).delete(any(Restaurant.class));
        service.deleteRestaurant(new Restaurant());
    }

    @Test
    public void testFindRestaurantById() throws ServiceException {
        Restaurant restaurant = new Restaurant("nume", "adresa", "categorie", "contact", 4, 2, 5, 4.0);

        when(restaurantDAO.findById(1)).thenReturn(Optional.of(restaurant));
        assertEquals("nume", service.findByIdRestaurant(1).getName());
        assertEquals("adresa", service.findByIdRestaurant(1).getAddress());
    }

    @Test
    public void testFindRestaurants() {
        Restaurant restaurant1 = new Restaurant("nume1", "adresa1", "categorie", "contact1", 5, 1, 4, 4);
        Restaurant restaurant2 = new Restaurant("nume2", "adresa2", "categorie", "contact2", 7, 4, 4, 4);
        Restaurant restaurant3 = new Restaurant("nume3", "adresa3", "categorie", "contact3", 8, 2, 4, 4);
        List<Restaurant> restaurants = new ArrayList<>();
        restaurants.add(restaurant1);
        restaurants.add(restaurant2);
        restaurants.add(restaurant3);
        when(restaurantDAO.findAll()).thenReturn(restaurants);
        assertEquals(service.findAllRestaurants().size(), 3);
        assertEquals(service.findAllRestaurants().get(0).getName(), "nume1");
        assertEquals(service.findAllRestaurants().get(0).getAddress(), "adresa1");
    }

    @Test
    public void testSaveUser() {
        doAnswer((Answer<User>) invocationOnMock -> null).when(userDAO).save(any(User.class));
        service.saveUser(new User());
    }

    @Test
    public void testDeleteUser() {
        doAnswer((Answer<User>) invocationOnMock -> null).when(userDAO).delete(any(User.class));
        service.deleteUser(new User());
    }

    @Test
    public void testFindUserById() throws ServiceException {
        User user = new User(1, "username", "email", "password", "role");
        when(userDAO.findById(1)).thenReturn(Optional.of(user));
        assertEquals("username", service.findByIdUser(1).getUsername());
        assertEquals("email", service.findByIdUser(1).getEmail());
        assertEquals("password", service.findByIdUser(1).getPassword());
        assertEquals("role", service.findByIdUser(1).getRole());
    }

    @Test
    public void testFindUser() {
        User user1 = new User(1, "username1", "email1", "password1", "role1");
        User user2 = new User(2, "username2", "email2", "password2", "role2");
        User user3 = new User(3, "username3", "email3", "password3", "role3");
        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        when(userDAO.findAll()).thenReturn(users);
        assertEquals(service.findAllUsers().size(), 3);
        assertEquals(service.findAllUsers().get(0).getUsername(), "username1");
        assertEquals(service.findAllUsers().get(0).getPassword(), "password1");
        assertEquals(service.findAllUsers().get(0).getRole(), "role1");
        assertEquals(service.findAllUsers().get(0).getEmail(), "email1");
    }

    @Test
    public void testSaveBooking() {
        doAnswer((Answer<Booking>) invocationOnMock -> null).when(bookingDAO).save(any(Booking.class));
        service.saveBooking(new Booking(user.getUserId(), restaurant.getRestaurantID(), noOfSeats, new Date(), false));
    }

    @Test
    public void testDeleteBooking() {
        doAnswer((Answer<Booking>) invocationOnMock -> null).when(bookingDAO).delete(any(Booking.class));
        service.deleteBooking(new Booking(user.getUserId(), restaurant.getRestaurantID(), noOfSeats, new Date(), false));
    }

    @Test
    public void testFindBookingId() throws ServiceException {
        Booking booking = new Booking(1, 1, 10, new Date(2019, 02, 11), true);
        when(bookingDAO.findById(1)).thenReturn(Optional.of(booking));
        assertEquals(1, service.findByIDBooking(1).getUserID());
        assertEquals(1, service.findByIDBooking(1).getRestaurantID());
        assertEquals(10, service.findByIDBooking(1).getNoOfSeats());
        assertEquals(2019, service.findByIDBooking(1).getStartDateTime().getYear());
        assertEquals(2, service.findByIDBooking(1).getStartDateTime().getMonth());
        assertEquals(11, service.findByIDBooking(1).getStartDateTime().getDate());
        assertEquals(true, service.findByIDBooking(1).isExpired());
    }

    @Test
    public void testFindBookings() {

        Booking booking1 = new Booking(1, 1, 10, new Date(2019, 02, 11), true);
        Booking booking2 = new Booking(2, 2, 20, new Date(2019, 03, 12), false);
        Booking booking3 = new Booking(3, 3, 30, new Date(2019, 04, 13), false);
        Booking booking4 = new Booking(4, 4, 40, new Date(2019, 05, 14), true);
        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking1);
        bookings.add(booking2);
        bookings.add(booking3);
        bookings.add(booking4);
        when(bookingDAO.findAll()).thenReturn(bookings);

        assertEquals(service.findAllBookings().size(), 4);
        assertEquals(service.findAllBookings().get(0).getUserID(), 1);
        assertEquals(service.findAllBookings().get(0).getRestaurantID(), 1);
        assertEquals(service.findAllBookings().get(0).getNoOfSeats(), 10);
        assertEquals(service.findAllBookings().get(0).getStartDateTime().getYear(), 2019);
        assertEquals(service.findAllBookings().get(0).getStartDateTime().getMonth(), 2);
        assertEquals(service.findAllBookings().get(0).getStartDateTime().getDate(), 11);
        assertTrue(service.findAllBookings().get(0).isExpired());

        assertEquals(service.findAllBookings().get(1).getUserID(), 2);
        assertEquals(service.findAllBookings().get(1).getRestaurantID(), 2);
        assertEquals(service.findAllBookings().get(1).getNoOfSeats(), 20);
        assertEquals(service.findAllBookings().get(1).getStartDateTime().getYear(), 2019);
        assertEquals(service.findAllBookings().get(1).getStartDateTime().getMonth(), 3);
        assertEquals(service.findAllBookings().get(1).getStartDateTime().getDate(), 12);
        assertFalse(service.findAllBookings().get(1).isExpired());

        assertEquals(service.findAllBookings().get(2).getUserID(), 3);
        assertEquals(service.findAllBookings().get(2).getRestaurantID(), 3);
        assertEquals(service.findAllBookings().get(2).getNoOfSeats(), 30);
        assertEquals(service.findAllBookings().get(2).getStartDateTime().getYear(), 2019);
        assertEquals(service.findAllBookings().get(2).getStartDateTime().getMonth(), 4);
        assertEquals(service.findAllBookings().get(2).getStartDateTime().getDate(), 13);
        assertFalse(service.findAllBookings().get(2).isExpired());

        assertEquals(service.findAllBookings().get(3).getUserID(), 4);
        assertEquals(service.findAllBookings().get(3).getRestaurantID(), 4);
        assertEquals(service.findAllBookings().get(3).getNoOfSeats(), 40);
        assertEquals(service.findAllBookings().get(3).getStartDateTime().getYear(), 2019);
        assertEquals(service.findAllBookings().get(3).getStartDateTime().getMonth(), 5);
        assertEquals(service.findAllBookings().get(3).getStartDateTime().getDate(), 14);
        assertTrue(service.findAllBookings().get(3).isExpired());
    }
}
