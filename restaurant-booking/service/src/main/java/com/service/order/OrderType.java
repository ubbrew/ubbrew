package com.service.order;

public enum OrderType {
    ASCENDING, DESCENDING
}
