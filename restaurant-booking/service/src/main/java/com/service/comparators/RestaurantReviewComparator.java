package com.service.comparators;

import com.model.entities.Restaurant;

import java.util.Comparator;
import java.util.Objects;

public class RestaurantReviewComparator implements Comparator<Restaurant> {

    @Override
    public int compare(Restaurant restaurant1, Restaurant restaurant2) {
        Double review1 = restaurant1.getReview();
        Double review2 = restaurant2.getReview();
        if (Objects.equals(review1, review2))
            return restaurant1.getName().compareTo(restaurant2.getName());
        if ((review2 - review1) < 0)
            return 1;
        return -1;
    }
}
