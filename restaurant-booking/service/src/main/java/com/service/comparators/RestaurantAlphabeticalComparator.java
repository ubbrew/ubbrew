package com.service.comparators;

import com.model.entities.Restaurant;

import java.util.Comparator;

public class RestaurantAlphabeticalComparator implements Comparator<Restaurant> {
    @Override
    public int compare(Restaurant restaurant1, Restaurant restaurant2) {
        return restaurant1.getName().compareTo(restaurant2.getName());
    }
}
