package com.service.comparators;

import com.model.entities.Restaurant;

import java.util.Comparator;
import java.util.Objects;

public class RestaurantEmptySeatsComparator implements Comparator<Restaurant> {

    @Override
    public int compare(Restaurant restaurant1, Restaurant restaurant2) {
        Integer review1 = restaurant1.getNo_of_empty_seats();
        Integer review2 = restaurant2.getNo_of_empty_seats();
        if (Objects.equals(review1, review2))
            return restaurant1.getName().compareTo(restaurant2.getName());
        if ((review2 - review1) < 0)
            return 1;
        return -1;
    }

}
