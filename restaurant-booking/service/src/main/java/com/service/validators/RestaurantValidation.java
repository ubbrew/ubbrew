package com.service.validators;

import org.springframework.stereotype.Service;
import java.util.regex.Pattern;

@Service
public class RestaurantValidation {

    // return true(valid) or false(not)
    public static boolean isValidName(String name) {
        String nameRegex = "[^a-z A-Z0-9]";

        Pattern pat = Pattern.compile(nameRegex);

        if (name == null)
            return false;

        return pat.matcher(name).matches();
    }

    // return true(valid) or false(not)
    public static boolean isContactNr(String no) {
        String nameRegex = "(0/91)?[7-9][0-9]{9}";

        Pattern pat = Pattern.compile(nameRegex);

        if (no == null)
            return false;

        return pat.matcher(no).matches();
    }
}
