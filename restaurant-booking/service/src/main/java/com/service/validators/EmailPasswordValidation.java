package com.service.validators;

import org.springframework.stereotype.Service;

import java.util.regex.Pattern;
@Service
public class EmailPasswordValidation {


    //  desc:
    // (?=.*[0-9]) a digit must occur at least once
    // (?=.*[a-z]) a lower case letter must occur at least once
    // (?=.*[A-Z]) an upper case letter must occur at least once
    // (?=.*[@#$%^&+=]) a special character must occur at least onc
    // (?=\\S+$) no whitespace allowed in the entire string
    // .{8,} at least 8 characters
    // return true or false
    public static boolean isValidPassword(String passwd) {
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";

        Pattern pat = Pattern.compile(pattern);

        if (passwd == null)
            return false;

        return pat.matcher(passwd).matches();
    }

    // return true or false
    public static boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);

        if (email == null)
            return false;

        return pat.matcher(email).matches();
    }
}
