package com.service.validators;

import org.springframework.stereotype.Service;

@Service
public class GeneralValidation {
    // return true(valid) or false(not)
    public static boolean isPositive(int val) {
        if(val<0){return false;}
        return true;
    }
}
