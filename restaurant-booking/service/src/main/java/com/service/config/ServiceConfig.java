package com.service.config;

import com.model.config.DAOConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(DAOConfig.class)
public class ServiceConfig {
}
