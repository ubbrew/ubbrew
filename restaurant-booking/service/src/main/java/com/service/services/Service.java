package com.service.services;

import com.model.dao.BookingDAO;
import com.model.dao.RestaurantDAO;
import com.model.dao.UserDAO;
import com.model.entities.Booking;
import com.model.entities.Restaurant;
import com.model.entities.User;
import com.service.comparators.ComparatorType;
import com.service.comparators.RestaurantAlphabeticalComparator;
import com.service.comparators.RestaurantEmptySeatsComparator;
import com.service.comparators.RestaurantReviewComparator;
import com.service.exception.ServiceException;
import com.service.order.OrderType;
import com.service.responses.Response;
import com.service.validators.EmailPasswordValidation;
import com.service.validators.GeneralValidation;
import com.service.validators.RestaurantValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Transactional
@org.springframework.stereotype.Service
public class Service {
    @Autowired
    private BookingDAO bookingDAO;
    @Autowired
    private RestaurantDAO restaurantDAO;
    @Autowired
    private UserDAO userDAO;

    public List<Booking> findAllBookings() {
        return this.bookingDAO.findAll();
    }

    public Booking findByIDBooking(int bookingID) throws ServiceException {
        Optional<Booking> optionalBooking = bookingDAO.findById(bookingID);
        if (optionalBooking.isPresent()) {
            return optionalBooking.get();
        }
        throw new ServiceException("The searched booking does not exist");
    }

    public Booking saveBooking(Booking booking) {
        return this.bookingDAO.save(booking);
    }

    public void deleteBooking(Booking booking) {
        this.bookingDAO.delete(booking);
    }

    public List<Restaurant> findAllRestaurants() {
        return this.restaurantDAO.findAll();
    }

    public Restaurant findByIdRestaurant(int restaurantID) throws ServiceException {
        Optional<Restaurant> optionalRestaurant = restaurantDAO.findById(restaurantID);
        if (optionalRestaurant.isPresent()) {
            return optionalRestaurant.get();
        }
        throw new ServiceException("The searched restaurant does not exist");
    }

    public Restaurant saveRestaurant(Restaurant restaurant) throws ServiceException {
        if(RestaurantValidation.isValidName(restaurant.getName()) && RestaurantValidation.isContactNr(restaurant.getContact_nr())
        && GeneralValidation.isPositive(restaurant.getNoOfReviews()) && GeneralValidation.isPositive(restaurant.getNoOfEmptySeats())
        && GeneralValidation.isPositive(restaurant.getNo_of_seats())){
            return this.restaurantDAO.save(restaurant);
        }
        throw new ServiceException("Invalid");
    }

    public void deleteRestaurant(Restaurant restaurant) {
        this.restaurantDAO.delete(restaurant);
    }

    public List<User> findAllUsers() {
        return this.userDAO.findAll();
    }

    public User findByIdUser(int userID) throws ServiceException {

        Optional<User> optionalUser = userDAO.findById(userID);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        }
        throw new ServiceException("The searched user does not exist");
    }

    public Response saveUser(User user) {
        if(EmailPasswordValidation.isValidEmail(user.getEmail())&&EmailPasswordValidation.isValidPassword(user.getPassword())){
        return new Response("User " + user.getUsername() + "was added successfully!");}
        else{
            return new Response("Invalid email or password!");
        }
    }

    public void deleteUser(User user) {
        this.userDAO.delete(user);
    }

    public List<Restaurant> sortRestaurants(ComparatorType comparatorType, OrderType orderType) {
        List<Restaurant> restaurants = findAllRestaurants();
        if (comparatorType.equals(ComparatorType.ALPHABETICAL)) {
            RestaurantAlphabeticalComparator alphabeticalComparator = new RestaurantAlphabeticalComparator();
            if (orderType == OrderType.DESCENDING)
                restaurants.sort(alphabeticalComparator.reversed());
            else
                restaurants.sort(alphabeticalComparator);
        }
        if(comparatorType.equals(ComparatorType.REVIEW)){
            RestaurantReviewComparator reviewComparator = new RestaurantReviewComparator();
            if (orderType == OrderType.DESCENDING)
                restaurants.sort(reviewComparator.reversed());
            else
                restaurants.sort(reviewComparator);
        }
        if(comparatorType.equals((ComparatorType.EMPTY_SEATS))){
            RestaurantEmptySeatsComparator emptySeatsComparator = new RestaurantEmptySeatsComparator();
            if (orderType == OrderType.DESCENDING)
                restaurants.sort(emptySeatsComparator.reversed());
            else
                restaurants.sort(emptySeatsComparator);

        }
        return restaurants;
    }

    public synchronized Response makeReservation(Restaurant restaurant, User user, int noOfSeats) {
        if (noOfSeats <= 0)
            return new Response("Please select a positive number!");
        if (restaurant.getNoOfEmptySeats() >= noOfSeats) {
            Booking booking = new Booking(user.getUserId(), restaurant.getRestaurantID(), noOfSeats, new Date(), false);
            bookingDAO.save(booking);
            restaurant.setNoOfEmptySeats(restaurant.getNoOfEmptySeats() - noOfSeats);
            restaurantDAO.save(restaurant);
            return new Response("Reservation successful!");
        }

        return new Response("There are not " + noOfSeats + " available at this restaurant!");
    }
}
