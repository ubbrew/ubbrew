use ubbrewdb;
CREATE TABLE User(
user_id INT NOT NULL AUTO_INCREMENT,
username VARCHAR(30),
email VARCHAR(60),
password VARCHAR(20),
role VARCHAR(15),
email VARCHAR(20),
PRIMARY KEY(user_id));

CREATE TABLE Restaurant(
restaurant_id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(30),
no_of_seats INT,
no_of_empty_seats INT,
address VARCHAR(30),
category VARCHAR(30),
contact_nr VARCHAR(30),
no_of_reviews INT,
review FLOAT,
PRIMARY KEY(restaurant_id));

CREATE TABLE Booking(
booking_id INT NOT NULL AUTO_INCREMENT,
user_id INT NOT NULL,
 restaurant_id INT NOT NULL,
 no_of_seats INT,
 start_date_time TIME,
 is_expired BOOLEAN,
 status VARCHAR(20),
 PRIMARY KEY(booking_id),
 FOREIGN KEY(user_id)
 REFERENCES User(user_id)
 ON DELETE CASCADE
 ON UPDATE CASCADE,
 FOREIGN KEY(restaurant_id)
 REFERENCES Restaurant(restaurant_id)
 ON DELETE CASCADE
 ON UPDATE CASCADE);

