package com.model.dao;

import com.model.repository.BookingRepository;
import com.model.entities.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class BookingDAO {
    @Autowired
    private BookingRepository bookingRepository;

    public Booking save(Booking booking){
        return bookingRepository.save(booking);
    }

    public List<Booking> findAll(){
        return (List<Booking>) bookingRepository.findAll();
    }

    public Optional<Booking> findById(int bookingID){
        return bookingRepository.findById(bookingID);
    }

    public void delete (Booking booking){
        bookingRepository.delete(booking);
    }
}
