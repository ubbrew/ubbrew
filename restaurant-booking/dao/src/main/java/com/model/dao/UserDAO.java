package com.model.dao;
import com.model.repository.UserRepository;
import com.model.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class UserDAO {
    @Autowired
    private UserRepository userRepository;

    public User save(User user){
        return userRepository.save(user);
    }

    public List<User> findAll(){
        return (List<User>) userRepository.findAll();
    }

    public Optional<User> findById(int userID){
        return userRepository.findById(userID);
    }

    public void delete (User user){
        userRepository.delete(user);
    }


}
