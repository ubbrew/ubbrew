package com.model.dao;

import com.model.repository.RestaurantRepository;
import com.model.entities.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class RestaurantDAO {
    @Autowired
    private RestaurantRepository restaurantRepository;

    public Restaurant save(Restaurant restaurant){
        return restaurantRepository.save(restaurant);
    }

    public List<Restaurant> findAll(){
        return (List<Restaurant>) restaurantRepository.findAll();
    }

    public Optional<Restaurant> findById(int restaurantID){
        return restaurantRepository.findById(restaurantID);
    }

    public void delete (Restaurant restaurant){
        restaurantRepository.delete(restaurant);
    }

}
