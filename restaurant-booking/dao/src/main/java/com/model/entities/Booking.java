package com.model.entities;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import java.util.Date;
@Entity
@Table(name="booking")
public class Booking {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int booking_id;
    
    private int user_id;
    
    private int restaurant_id;
    
    private int no_of_seats;
    
    private Date start_date_time;
    
    private boolean is_expired;

    public Booking() { }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public int getNo_of_seats() {
        return no_of_seats;
    }

    public void setNo_of_seats(int no_of_seats) {
        this.no_of_seats = no_of_seats;
    }

    public Date getStart_date_time() {
        return start_date_time;
    }

    public void setStart_date_time(Date start_date_time) {
        this.start_date_time = start_date_time;
    }

    public boolean isIs_expired() {
        return is_expired;
    }

    public void setIs_expired(boolean is_expired) {
        this.is_expired = is_expired;
    }

    public Booking(int userID, int restaurantID, int noOfSeats, Date startDateTime, boolean isExpired) {
        this.user_id = userID;
        this.restaurant_id = restaurantID;
        this.no_of_seats = noOfSeats;
        this.start_date_time = startDateTime;
        this.is_expired = isExpired;
    }

    public int getBookingID() {
        return booking_id;
    }

    public void setBookingID(int bookingID) {
        this.booking_id = bookingID;
    }

    public int getUserID() {
        return user_id;
    }

    public void setUserID(int userID) {
        this.user_id = userID;
    }

    public int getRestaurantID() {
        return restaurant_id;
    }

    public void setRestaurantID(int restaurantID) {
        this.restaurant_id = restaurantID;
    }

    public int getNoOfSeats() {
        return no_of_seats;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.no_of_seats = noOfSeats;
    }

    public Date getStartDateTime() {
        return start_date_time;
    }

    public void setStartDateTime(Date startDateTime) {
        this.start_date_time = startDateTime;
    }

    public boolean isExpired() {
        return is_expired;
    }

    public void setExpired(boolean expired) {
        is_expired = expired;
    }
}
