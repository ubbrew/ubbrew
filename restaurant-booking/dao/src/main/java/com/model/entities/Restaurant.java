package com.model.entities;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Table(name = "restaurant")
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int restaurant_id;

    private String name;

    private int no_of_seats;

    private int no_of_empty_seats;

    private String address;

    private String category;

    private String contact_nr;

    private int no_of_reviews;

    private double review;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Restaurant( String name, String address, String category, String contactNr, int noOfSeats, int noOfEmptySeats, int noOfReviews, double review) {
        this.name = name;
        this.address = address;
        this.category = category;
        this.contact_nr = contactNr;
        this.no_of_seats = noOfSeats;
        this.no_of_empty_seats = noOfEmptySeats;
        this.no_of_reviews = noOfReviews;
        this.review = review;
    }

    public int getNoOfReviews() {
        return no_of_reviews;
    }

    public void setNoOfReviews(int noOfReviews) {
        this.no_of_reviews = noOfReviews;
    }

    public double getReview() {
        return review;
    }

    public void setReview(double review) {
        this.review = review;
    }

    public Restaurant() {
    }

    public int getRestaurantID() {
        return restaurant_id;
    }

    public void setRestaurantID(int restaurantID) {
        this.restaurant_id = restaurantID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNr() {
        return contact_nr;
    }

    public void setContactNr(String contactNr) {
        this.contact_nr = contactNr;
    }

    public int getNoOfSeats() {
        return no_of_seats;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.no_of_seats = noOfSeats;
    }

    public int getNoOfEmptySeats() {
        return no_of_empty_seats;
    }

    public void setNoOfEmptySeats(int noOfEmptySeats) {
        this.no_of_empty_seats = noOfEmptySeats;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public int getNo_of_seats() {
        return no_of_seats;
    }

    public void setNo_of_seats(int no_of_seats) {
        this.no_of_seats = no_of_seats;
    }

    public int getNo_of_empty_seats() {
        return no_of_empty_seats;
    }

    public void setNo_of_empty_seats(int no_of_empty_seats) {
        this.no_of_empty_seats = no_of_empty_seats;
    }

    public String getContact_nr() {
        return contact_nr;
    }

    public void setContact_nr(String contact_nr) {
        this.contact_nr = contact_nr;
    }

    public int getNo_of_reviews() {
        return no_of_reviews;
    }

    public void setNo_of_reviews(int no_of_reviews) {
        this.no_of_reviews = no_of_reviews;
    }
}
