package com.rest.controller;

import com.model.entities.Booking;
import com.model.entities.Restaurant;
import com.model.entities.User;
import com.service.comparators.ComparatorType;
import com.service.order.OrderType;
import com.service.responses.Response;
import com.service.services.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:8081")
@RestController
@EnableAutoConfiguration
public class Controller {
    @Autowired
    private Service service;

    @GetMapping("/restaurants")
    public List<Restaurant> findAllRestaurants() {
        return service.findAllRestaurants();
    }

    @GetMapping("/users")
    public List<User> findAllUsers() {
        return service.findAllUsers();
    }

    @GetMapping("/bookings")
    public List<Booking> findAllBookings() {
        return service.findAllBookings();
    }

    @GetMapping("/restaurant/{id}")
    public Restaurant findRestaurantById(@PathVariable int id) {
        return service.findByIdRestaurant(id);
    }

    @GetMapping("/booking/{id}")
    public Booking findBookingById(@PathVariable int id) {
        return service.findByIDBooking(id);
    }

    @GetMapping("/user/{id}")
    public User findUserById(@PathVariable int id) {
        return service.findByIdUser(id);
    }

    @PostMapping("/user")
    public Response addUser(@RequestBody User user) {
        return service.saveUser(user);
    }

    @PostMapping("/restaurant")
    public Restaurant addRestaurant(@RequestBody Restaurant restaurant) {
        return service.saveRestaurant(restaurant);
    }

    @PostMapping("/booking")
    public Booking addBooking(@RequestBody Booking booking) {
        return service.saveBooking(booking);
    }

    @PutMapping("/user/{id}")
    public Response updateUser(@PathVariable int id, User user) {
        user.setUserId(id);
        return service.saveUser(user);
    }

    @PutMapping("/restaurant/{id}")
    public Restaurant updateRestaurant(@PathVariable int id, Restaurant restaurant) {
        restaurant.setRestaurantID(id);
        return service.saveRestaurant(restaurant);
    }

    @PutMapping("/booking/{id}")
    public Booking updateBooking(@PathVariable int id, Booking booking) {
        booking.setBookingID(id);
        return service.saveBooking(booking);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable int id) {
        User user = service.findByIdUser(id);
        service.deleteUser(user);
    }

    @DeleteMapping("/restaurant/{id}")
    public void deleteRestaurant(@PathVariable int id) {
        Restaurant restaurant = service.findByIdRestaurant(id);
        service.deleteRestaurant(restaurant);
    }

    @DeleteMapping("/booking/{id}")
    public void deleteBooking(@PathVariable int id) {
        Booking booking = service.findByIDBooking(id);
        service.deleteBooking(booking);
    }

    @GetMapping("/restaurants/sorted")
    public List<Restaurant> sortRestaurants(@RequestParam ComparatorType comparatorType, @RequestParam OrderType orderType) {
        return service.sortRestaurants(comparatorType, orderType);
    }

    @PostMapping("/reservation")
    public Response makeReservation(@RequestParam int userId, @RequestParam int restaurantId, @RequestParam int noOfSeats) {
        Restaurant restaurant = service.findByIdRestaurant(restaurantId);
        User user = service.findByIdUser(userId);
        return service.makeReservation(restaurant, user, noOfSeats);
    }

}
